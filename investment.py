__author__ = 'mono'

company_name_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L']
current_price_list = [200, 300, 250, 100, 300, 50, 200, 300, 250, 100, 300, 50]
future_price_list = [250, 400, 400, 120, 330, 50, 100, 500, 750, 160, 300, 90]

current_price = []
future_price = []
profit = []
company_name = []
profit_by_company = []
final_company_list = []
final_investment_list = []
final_return_list = []

for i in xrange(len(company_name_list)):
    curr_price = current_price_list[i]
    fut_price = future_price_list[i]
    val = ((fut_price - curr_price) / float(curr_price)) * 100
    profit_by_company.append(val)

# print profit_by_company

while sum(final_investment_list) <= 1000:
    max_val = max(profit_by_company)
    max_index = profit_by_company.index(max_val)
    profit_by_company[max_index] = -1
    final_investment_list.append(current_price_list[max_index])
    final_return_list.append(future_price_list[max_index])
    final_company_list.append(company_name_list[max_index])

if sum(final_investment_list) > 1000:
    final_investment_list.pop()
    final_return_list.pop()
    final_company_list.pop()

print 'Max Investment with Min Risk:'
print final_company_list
# print final_investment_list
# print final_return_list
print ((sum(final_return_list) - sum(final_investment_list)) / float(sum(final_investment_list))) * 100
print '==================================================================================================='


def get_max_profit(c_price_list, f_price_list, c_name_list, target, partial_current_price=[], partial_future_price=[],
                   partial_company_name=[]):
    s = sum(partial_current_price)

    if s <= target:
        if partial_current_price and sum(partial_current_price) > 800:
            current_price.append(partial_current_price)
            future_price.append(partial_future_price)
            v = (sum(partial_future_price) - sum(partial_current_price)) / float(sum(partial_current_price)) * 100
            profit.append(v)
            company_name.append(partial_company_name)
    if s >= target:
        return

    for i in range(len(c_price_list)):
        n = c_price_list[i]
        f_n = f_price_list[i]
        f_nc = c_name_list[i]
        c_price_remaining = c_price_list[i + 1:]
        f_price_remaining = f_price_list[i + 1:]
        c_name_remaining = c_name_list[i + 1:]
        get_max_profit(c_price_remaining, f_price_remaining, c_name_remaining, target, partial_current_price + [n],
                       partial_future_price + [f_n], partial_company_name + [f_nc])
    return current_price, future_price, profit, company_name


current_price, future_price, profit, company_name = get_max_profit(current_price_list, future_price_list,
                                                                   company_name_list, 1000)
# print company_name
# print current_price
# print future_price
# print profit
print '\n'
print 'Max Profit:'
print company_name[profit.index(max(profit))]
print max(profit)
